export enum Role {
  applicant = "APPLICANT",
  employer = "EMPLOYER",
  admin = "ADMIN",
  staffer = "STAFFER",
  adminStaff = "ADMINSTAFF"
}

// DONOT CHANGE THE ABOVE NAMES AS THE ROUTING GUARD DEPENDS ON THEM
